"""event URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from frontend import views as frontend_views

extra_patterns = [
    url(r'^$', frontend_views.home),
    url(r'^register$', frontend_views.register, name='register'),
    url(r'^informasiumum$', frontend_views.panitia, name='informasiumum'),
    url(r'^abstract$', frontend_views.abstract, name='abstract'),
    url(r'^infohotel$', frontend_views.infohotel, name='infohotel'),
    url(r'^scientificprogramme$', frontend_views.scientificprogramme, name='scientificprogramme'),
    url(r'^congressworkshop$', frontend_views.congressworkshop, name='congressworkshop'),
    url(r'^sambutan$', frontend_views.welcome, name='welcome'),
    url(r'^biayapendaftaran$', frontend_views.biayapendaftaran, name='biayapendaftaran'),
    url(r'^cararegistrasi$', frontend_views.cararegistrasi, name='cararegistrasi'),
    url(r'^panitia$', frontend_views.panitia, name='panitia'),
    url(r'^konfirmasi_pembayaran$', frontend_views.konfirmasi_pembayaran, name='konfirmasi_pembayaran'),
    url(r'^sent_nomor_registration/(?P<id>[0-9]+)/$', frontend_views.sent_nomor_registration, name='sent_nomor_registration'),
]

urlpatterns = [
	url(r'^', include(extra_patterns)),

    url(r'^admin/', admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
