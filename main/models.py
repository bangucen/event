from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Hotel(models.Model):
	name = models.CharField(max_length=255, db_index=True, verbose_name="Nama Hotel")
	room_type = models.CharField(max_length=255, db_index=True, verbose_name="Tipe Kamar")
	price = models.FloatField(default=0.0, verbose_name="Harga Kamar")
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __unicode__(self):
		return str(self.name)
