from __future__ import unicode_literals

from django.db import models
from accounts.models import *
from main.models import *
from workshop.models import *

# Create your models here.
class Registration(models.Model):
	participant = models.ForeignKey(Participant, null=True, on_delete=models.SET_NULL)
	# hotel = models.ForeignKey(Hotel, null=True, blank=True, on_delete=models.SET_NULL)
	hotels = models.ManyToManyField(Hotel, db_index=True)
	stand_alones = models.ManyToManyField(StandAlone, db_index=True)
	meeting_package = models.ForeignKey(MeetingPackage, null=True, on_delete=models.SET_NULL, blank=True)
	no_registrasi = models.CharField(max_length=255, null=True, db_index=True)
	no_invoice = models.CharField(max_length=255, null=True, db_index=True)
	include_hotel = models.BooleanField(default=False)
	detail_hotel = models.TextField(null=True, blank=True)
	check_in_hotel = models.DateField(null=True, blank=True)
	check_out_hotel = models.DateField(null=True, blank=True)
	total_nights = models.IntegerField(default=0)
	total_accomodation_payment = models.FloatField(default=0.0)
	total_registration = models.FloatField(default=0.0)
	detail_registration = models.TextField()
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return str(self.participant)

	def split_detail_registration(self):

		results = self.detail_registration.split('\n')

		return results

	def total_biaya(self):
		total = self.total_registration + self.total_accomodation_payment
		return total

	def save(self, *args, **kwargs):
		registrations = Registration.objects.all().order_by('-id')

		if len(registrations) == 0:
			self.no_registrasi = "ON0001"
		else:
			self.no_registrasi = "ON000%s" % (int(registrations[0].id) + 1)
		# do_something here
		return super(Registration, self).save(*args, **kwargs)

class KonfirmasiPembayaran(models.Model):
	registration = models.ForeignKey(Registration)
	bank_tujuan = models.CharField(max_length=255, null=True, db_index=True)
	bank_pengirim = models.CharField(max_length=255, null=True, db_index=True)
	no_rekening_pengirim = models.CharField(max_length=255, null=True, db_index=True)
	nama_pengirim = models.CharField(max_length=255, null=True, db_index=True)
	tanggal_transfer = models.DateField()
	jumlah_transfer = models.FloatField(default=0.0)
	email = models.CharField(max_length=255, null=True, db_index=True)
	bukti_transfer = models.ImageField(upload_to='bukti_transfer', null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now_add=True)
