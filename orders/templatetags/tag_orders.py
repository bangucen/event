from django import template

register = template.Library()

@register.filter(name='get_text_detail')
def get_text_detail(value):
	split_value = value.split('(Rp. ')
	return split_value[0]

@register.filter(name='get_price_detail')
def get_price_detail(value):
	split_value = value.split('(Rp. ')
	return split_value[1].replace(')', '')

@register.filter(name='total_harga_kamar')
def total_harga_kamar(harga_kamar, jumlah_malam):
	return int(harga_kamar) * int(jumlah_malam)
