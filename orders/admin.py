from django.contrib import admin
from django.utils.html import format_html
from .models import *

class KonfirmasiPembayaranAdmin(admin.ModelAdmin):
	change_list_template = 'admin/orders/konfirmasi_pembayaran_list_admin.html'

	list_display = ('registration', 'sent_email')

	def sent_email(self, obj):
		return format_html("<a href='/sent_nomor_registration/{0}' style='border: none;font-weight: 400;background: #417690;height: 35px;line-height: 15px;background: #79aec8;padding: 5px 5px;border: none;border-radius: 4px;color: #fff;cursor: pointer;'>Kirim No. Registrasi</a>", obj.id)

	sent_email.allow_tags = True

# Register your models here.
admin.site.register(Registration)
admin.site.register(KonfirmasiPembayaran, KonfirmasiPembayaranAdmin)
