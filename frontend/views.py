from django.shortcuts import render, redirect
from django.http import HttpResponse

from accounts.models import *
from orders.models import *
from main.models import *
from workshop.models import *

from django.core.mail import send_mail
from django.template import RequestContext, loader
from django.template.loader import render_to_string
from django.conf import settings
from datetime import *

import random

# Create your views here.


def home(request):
    return render(request, 'index.html', {})

def panitia(request):
    return render(request, 'panitia.html', {})

def abstract(request):
    return render(request, 'abstract.html', {})

def scientificprogramme(request):
    return render(request, 'scientificprogramme.html', {})

def congressworkshop(request):
    return render(request, 'congressworkshop.html', {})

def welcome(request):
    return render(request, 'welcome.html', {})

def infohotel(request):
    return render(request, 'infohotel.html', {})

def biayapendaftaran(request):
    return render(request, 'biayapendaftaran.html', {})

def cararegistrasi(request):
    return render(request, 'cararegistrasi.html', {})

def register(request):

	context = {}

	context['hotels'] = Hotel.objects.all().order_by('price')
	context['stand_alones'] = StandAlone.objects.order_by('id')
	context['meeting_packages'] = MeetingPackage.objects.order_by('id')

	if request.POST:

		title = request.POST.get('title')
		type = request.POST.get('type')
		package = request.POST.get('package')
		full_name = request.POST.get('full_name')
		institution = request.POST.get('institution')
		address = request.POST.get('address')
		city = request.POST.get('city')
		post_code = request.POST.get('post_code')
		phone = request.POST.get('phone')
		fax = request.POST.get('fax')
		email = request.POST.get('email')
		sponsor_full_name = request.POST.get('sponsor_full_name', "")
		sponsor_position = request.POST.get('sponsor_position')
		sponsor_title = request.POST.get('sponsor_title')
		sponsor_city = request.POST.get('sponsor_city')
		sponsor_phone = request.POST.get('sponsor_phone')
		sponsor_email = request.POST.get('sponsor_email')
		add_hotel = request.POST.get('add_hotel')
		datefilter = request.POST.get('datefilter')
		total_nights = request.POST.get('total_nights')
		hotel_ids = request.POST.getlist('hotel_choice')
		meeting_package_id = request.POST.get('meeting_package')
		stand_alone_ids = request.POST.getlist('stand_alone')

		# untuk detail pesanan
		hwk1 = request.POST.get('hwk1', 0)
		hwk2 = request.POST.get('hwk2', 0)
		hwk3 = request.POST.get('hwk3', 0)
		hwk4 = request.POST.get('hwk4', 0)
		hwk5 = request.POST.get('hwk5', 0)
		hwk6 = request.POST.get('hwk6', 0)
		hwk7 = request.POST.get('hwk7', 0)
		hwk1_text = request.POST.get('hwk1_text', "")
		hwk2_text = request.POST.get('hwk2_text', "")
		hwk3_text = request.POST.get('hwk3_text', "")
		hwk4_text = request.POST.get('hwk4_text', "")
		hwk5_text = request.POST.get('hwk5_text', "")
		hwk6_text = request.POST.get('hwk6_text', "")
		hwk7_text = request.POST.get('hwk7_text', "")

		date_register = datetime.now().date()

		try:

			if sponsor_full_name != "":

				sponsor = Sponsor()
				sponsor.title = sponsor_title
				sponsor.full_name = sponsor_full_name
				sponsor.position = sponsor_position
				sponsor.company = sponsor_city
				sponsor.city = sponsor_city
				sponsor.phone = sponsor_phone
				sponsor.email = sponsor_email
				sponsor.save()

				peserta = Participant()
				peserta.title = title
				peserta.type = type
				peserta.package = package
				peserta.full_name = full_name
				peserta.institution = institution
				peserta.address = address
				peserta.city = city
				peserta.post_code = post_code
				peserta.phone = phone
				peserta.email = email
				peserta.sponsor = sponsor
				peserta.save()
			else:
				peserta = Participant()
				peserta.title = title
				peserta.type = type
				peserta.package = package
				peserta.full_name = full_name
				peserta.institution = institution
				peserta.address = address
				peserta.city = city
				peserta.post_code = post_code
				peserta.phone = phone
				peserta.email = email
				peserta.save()

			text_detail_hotel = ""
			total_keseluruhan_hotel = 0
			text_detail = ""
			total_keseluruhan = 0

			# if hwk1 != "":
			# 	total_keseluruhan += int(hwk1)
			# 	text_detail += "- %s (Rp. %s)\n" % (hwk1_text, hwk1)

			# if hwk2 != "":
			# 	total_keseluruhan += int(hwk2)
			# 	text_detail += "- %s (Rp. %s)\n" % (hwk2_text, hwk2)

			# if hwk3 != "":
			# 	total_keseluruhan += int(hwk3)
			# 	text_detail += "- %s (Rp. %s)\n" % (hwk3_text, hwk3)

			# if hwk4 != "":
			# 	total_keseluruhan += int(hwk4)
			# 	text_detail += "- %s (Rp. %s)\n" % (hwk4_text, hwk4)

			list_hotels = []
			list_stand_alones = []

			for i in stand_alone_ids:
				x = StandAlone.objects.get(pk=i)
				list_stand_alones.append(x)

				for z in x.date_workshopes.all():
					if z.start_date and z.end_date:
						if date_register >= z.start_date and date_register <= z.end_date:
							total_keseluruhan += int(z.price)
							text_detail += "- %s (Rp. %s)\n" % (x.name, z.price)

			if add_hotel == 'on':
				datefilter_split = datefilter.split('-')
				parse_start = datetime.strptime(datefilter_split[0].strip(), '%m/%d/%Y')
				parse_end = datetime.strptime(datefilter_split[1].strip(), '%m/%d/%Y')

				for i in hotel_ids:
					hotel_select = Hotel.objects.get(pk=i)
					list_hotels.append(hotel_select)
					total_keseluruhan_hotel += int(total_nights) * hotel_select.price

				# if hwk5 != "":
				# 	total_keseluruhan_hotel += int(hwk5)
				# 	text_detail_hotel += "- %s (Rp. %s)\n" % (hwk5_text, hwk5)
				# if hwk6 != "":
				# 	total_keseluruhan_hotel += int(hwk6)
				# 	text_detail_hotel += "- %s (Rp. %s)\n" % (hwk6_text, hwk6)
				# if hwk7 != "":
				# 	total_keseluruhan_hotel += int(hwk7)
				# 	text_detail_hotel += "- %s (Rp. %s)\n" % (hwk7_text, hwk7)

			generate_invoice = '%32x' % random.getrandbits(16*8)
			angka_unik = random.randint(100, 1000)
			
			register = Registration()
			register.participant = peserta
			register.no_invoice = generate_invoice[5:10].upper() + str(angka_unik)

			if meeting_package_id:
				meeting_package = MeetingPackage.objects.get(pk=meeting_package_id)
				for z in meeting_package.date_workshopes.all():
					if z.start_date and z.end_date:
						if date_register >= z.start_date and date_register <= z.end_date:
							total_keseluruhan += int(z.price)
							text_detail += "- %s (Rp. %s)\n" % (meeting_package.name, z.price)
				register.meeting_package_id = meeting_package_id

			if add_hotel == 'on':
				register.check_in_hotel = parse_start
				register.check_out_hotel = parse_end
				register.detail_hotel = text_detail_hotel
				register.total_accomodation_payment = total_keseluruhan_hotel
				register.total_nights = total_nights
				register.include_hotel = True
			register.total_registration = total_keseluruhan + angka_unik
			register.detail_registration = text_detail

			register.save()

			register.hotels.add(*list_hotels)
			register.stand_alones.add(*list_stand_alones)
			
			#send_mail('Pendaftaran Online Kongres Nasional Peralmuni Surabaya 2017','message','huseinmuhamad@gmail.com',[email],fail_silently=False)
			context_email = {
				'email': email,
				'registration': register,
				'message_success': 'Terima Kasih telah mendaftar sebagai peserta'
			}

			html_msg = loader.render_to_string('email_confirmation.html', context_email)
			confirm_title = 'Pendaftaran Online Kongres Nasional Peralmuni Surabaya 2017'

			send_mail(confirm_title, '', settings.EMAIL_HOST_USER,[email], html_message=html_msg)
		except Exception, e:
			return HttpResponse("%s" % e)
		else:
			context = {
				'message_success': 'Registrasi berhasil silakan cek email untuk rincian pembayaran'
			}

			context['hotels'] = Hotel.objects.all().order_by('price')
			context['stand_alones'] = StandAlone.objects.order_by('id')
			context['meeting_packages'] = MeetingPackage.objects.order_by('id')

			return render(request, 'registration.html', context)

	return render(request, 'registration.html', context)

def konfirmasi_pembayaran(request):

    if request.POST:

        no_invoice = request.POST.get('no_invoice')
        bank_tujuan = request.POST.get('bank_tujuan')
        bank_pengirim = request.POST.get('bank_pengirim')
        no_rekening_pengirim = request.POST.get('no_rekening_pengirim')
        nama_pengirim = request.POST.get('nama_pengirim')
        tanggal_transfer = request.POST.get('tanggal_transfer')
        jumlah_transfer = request.POST.get('jumlah_transfer').replace(',' , '').replace('.' , '')
        bukti_transfer = request.FILES['bukti_transfer']
        email = request.POST.get('email')

        try:
            registration = Registration.objects.get(no_invoice=no_invoice)
        except Exception, e:

            context = {
                'message_error': 'Nomor Invoice yang dimasukkan tidak ditemukan'
            }

            return render(request, 'konfirmasi_pembayaran.html', context)
        else:

            parse_tgl_transfer = datetime.strptime(
                tanggal_transfer, '%m/%d/%Y')

            kp = KonfirmasiPembayaran()
            kp.registration = registration
            kp.no_invoice = no_invoice
            kp.bank_tujuan = bank_tujuan
            kp.bank_pengirim = bank_pengirim
            kp.no_rekening_pengirim = no_rekening_pengirim
            kp.nama_pengirim = nama_pengirim
            kp.tanggal_transfer = parse_tgl_transfer
            kp.jumlah_transfer = jumlah_transfer
            kp.bukti_transfer = bukti_transfer
            kp.email = email
            kp.save()

            context = {
                'message_success': 'Konfirmasi Pembayaran berhasil, tunggu kami cek terlebih dahulu'
            }

            return render(request, 'konfirmasi_pembayaran.html', context)

    return render(request, 'konfirmasi_pembayaran.html', {})


def sent_nomor_registration(request, id):

    kp = KonfirmasiPembayaran.objects.get(id=id)

    context = {
        'email': kp.registration.participant.email,
        'registration': kp.registration
    }

    html_msg = loader.render_to_string('email_noregistration.html', context)
    confirm_title = 'Penerimaan Pembayaran'

    send_mail(confirm_title, '', '', [
              kp.registration.participant.email], html_message=html_msg)

    return redirect('/admin/orders/konfirmasipembayaran/')
