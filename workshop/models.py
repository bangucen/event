from __future__ import unicode_literals

from django.db import models

class ParticipantType(models.Model):
	CHOICE = (
		('0', 'Student'),
		('1', 'GP'),
		('2', 'Spesialis'),
	)

	name = models.CharField(max_length=255, db_index=True, choices=CHOICE)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __unicode__(self):
		return self.get_name_display()


class DateWorkshop(models.Model):
	start_date = models.DateField(null=True, blank=True)
	end_date = models.DateField(null=True, blank=True)
	price = models.FloatField(default=0.0)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __unicode__(self):
		return '%s - %s, harga: %s' % (self.start_date if self.start_date else '', self.end_date if self.end_date else '', self.price)

	def meeting_package(self):
		if self.meetingpackage_set.all():
			return self.meetingpackage_set.all()[0].name
		else:
			return '-'

	def stand_alone_package(self):
		if self.standalone_set.all():
			return self.standalone_set.all()[0].name
		else:
			return '-'

class StandAlone(models.Model):
	name = models.CharField(max_length=255, db_index=True, verbose_name="Nama Paket")
	participant_types = models.ManyToManyField(ParticipantType, blank=True)
	date_workshopes = models.ManyToManyField(DateWorkshop)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __unicode__(self):
		return self.name

	def list_participany_types(self):
		if self.participant_types:
			list_ = []
			for i in self.participant_types.all():
				list_.append(i.__unicode__())
			return ",".join(list_)
		else:
			return '-'


class MeetingPackage(models.Model):
	name = models.CharField(max_length=255, db_index=True, verbose_name="Nama Paket")
	participant_type = models.ForeignKey(ParticipantType, null=True)
	date_workshopes = models.ManyToManyField(DateWorkshop)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __unicode__(self):
		return self.name
