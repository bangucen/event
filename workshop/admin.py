from django.contrib import admin
from .models import *


class StandAloneAdmin(admin.ModelAdmin):
	list_display = ('__unicode__',)


class DateWorkshopAdmin(admin.ModelAdmin):
	list_display = ('start_date', 'end_date', 'meeting_package', 'stand_alone_package', 'price')

admin.site.register(StandAlone, StandAloneAdmin)
admin.site.register(DateWorkshop, DateWorkshopAdmin)
admin.site.register(ParticipantType)
admin.site.register(MeetingPackage)

# Register your models here.
