# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-17 08:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(db_index=True, max_length=255)),
                ('full_name', models.CharField(db_index=True, max_length=255)),
                ('institution', models.CharField(db_index=True, max_length=255)),
                ('address', models.TextField()),
                ('city', models.CharField(db_index=True, max_length=255)),
                ('post_code', models.CharField(db_index=True, max_length=255)),
                ('phone', models.CharField(db_index=True, max_length=255)),
                ('email', models.CharField(db_index=True, max_length=255)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Sponsor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(db_index=True, max_length=255)),
                ('full_name', models.CharField(db_index=True, max_length=255)),
                ('company', models.CharField(db_index=True, max_length=255)),
                ('city', models.CharField(db_index=True, max_length=255)),
                ('phone', models.CharField(db_index=True, max_length=255)),
                ('email', models.CharField(db_index=True, max_length=255)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
