from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Sponsor(models.Model):
	title = models.CharField(max_length=255, db_index=True)
	full_name = models.CharField(max_length=255, db_index=True)
	company = models.CharField(max_length=255, db_index=True)
	position = models.CharField(max_length=255, db_index=True, null=True, blank=True)
	city = models.CharField(max_length=255, db_index=True)
	phone = models.CharField(max_length=255, db_index=True)
	email = models.CharField(max_length=255, db_index=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now_add=True)

class Participant(models.Model):
	sponsor = models.OneToOneField(Sponsor, null=True, blank=True)
	type = models.CharField(max_length=255, null=True, blank=True, db_index=True)
	package = models.CharField(max_length=255, null=True, blank=True, db_index=True)
	title = models.CharField(max_length=255, db_index=True)
	full_name = models.CharField(max_length=255, db_index=True)
	institution = models.CharField(max_length=255, db_index=True)
	address = models.TextField()
	city = models.CharField(max_length=255, db_index=True)
	post_code = models.CharField(max_length=255, db_index=True)
	phone = models.CharField(max_length=255, db_index=True)
	email = models.CharField(max_length=255, db_index=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return str(self.full_name)
